package com.epam.customDecorator;

import com.epam.helper.Waiter;
import org.openqa.selenium.WebElement;

public class Button extends Element {

    public Button(WebElement webElement) {
        super(webElement);
    }

    public void clickIfButtonClickable() {
        Waiter.waitForElementPresent(getWebElement()).click();
    }
}
