package com.epam.helper;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.helper.Constanta.WAIT_SLEEP_TIME;
import static com.epam.helper.Constanta.WAIT_TIME_OUT;

public class Waiter {

    public Waiter() {
    }

    public static WebElement waitForElementPresent(WebElement element) {
        return new WebDriverWait(DriverProvider.getDriver(), WAIT_TIME_OUT, WAIT_SLEEP_TIME )
                .ignoring(StaleElementReferenceException.class, ElementNotInteractableException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
