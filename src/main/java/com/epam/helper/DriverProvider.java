package com.epam.helper;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class DriverProvider {
    private final static int TIMEOUT = 90;
    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public DriverProvider() {
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(driver.get())) {
            driver.set(new ChromeDriver());
            setUpDriver(driver);
        }
        return driver.get();
    }

    private static void setUpDriver(ThreadLocal<WebDriver> driver) {
        driver.get().manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
        driver.get().manage().window().maximize();
    }

    public static void closeDriver() {
        if (!Objects.isNull(driver.get())) {
            driver.get().quit();
            driver.remove();
        }
    }
}
