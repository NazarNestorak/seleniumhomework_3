package com.epam.gmailPO;

import com.epam.gmailBO.GmailPageBO;
import com.epam.helper.DriverProvider;
import com.epam.model.Letter;
import com.epam.model.User;
import org.testng.Assert;
import org.testng.annotations.*;
import java.util.Iterator;
import java.util.stream.Stream;

import static com.epam.helper.Constanta.RECIPIENT;
import static com.epam.helper.Constanta.SUBJECT;
import static com.epam.helper.Constanta.TEXT;
import static com.epam.helper.Helper.*;

public class TestGmail {

    @BeforeClass
    public void setUp() {
        System.setProperty(WEBDRIVER, WEBDRIVER_PATH);
    }

    @DataProvider(parallel = true)
    public Iterator<Object[]> users() {
        return Stream.of(
                new Object[]{new User(LOGIN,PASSWORD)},
                new Object[]{new User(LOGIN_1,PASSWORD)},
                new Object[]{new User(LOGIN_2,PASSWORD)},
                new Object[]{new User(LOGIN_3,PASSWORD)}
        ).iterator();
    }

    @Test(dataProvider = "users")
    public void testAccountLogin(User user) throws InterruptedException {
        Letter letter = new Letter(RECIPIENT,SUBJECT,TEXT);
        GmailPageBO gmailPageBO = new GmailPageBO();
        DriverProvider.getDriver().get(GMAIL_LOGIN_PAGE_URL);
        gmailPageBO.logging(user.getLogin(), user.getPassword());
        gmailPageBO.writeAndSendLetter(letter);
        gmailPageBO.openLastSendMessage();
        Assert.assertTrue(letter.getSubject().equals(gmailPageBO.getTitle()),"not find needed letter ");
    }

    @AfterMethod
    public void closeDriverProvider() {
        DriverProvider.closeDriver();
    }
}
